# APIManager

Network Layer с использованием URLSession и Async/Await

Перед использованием нужно добавить URL к API:

```
import APIManager

extension Endpoint {
    var url: URL? {
        let url = API.baseURL
        return URL(string: url + path)
    }
}
```

**Пример использования:**

**Store:**

```
import APIManager

protocol SearchDataServiceProtocol {
    func getHotels(with data: SearchHotelData) async throws -> HotelsInfo
}

class SearchDataService: SearchDataServiceProtocol {
    static let searchDataService = SearchDataService()

    func getHotels(with data: SearchHotelData) async throws -> HotelsInfo {
        guard let result = await APIManager().sendRequest(
            model: HotelsInfo.self,
            endpoint:  SearchDataServiceEndpoint.getHotels(with: data)
        ) else {
            throw RequestError.statusNotOk
        }

        switch result {
        case .success(let response):
            return response
        case .failure:
            throw RequestError.statusNotOk
        }
    }
}
```

**Fetch data**

@MainActor
final class HotelsSearchVCModel {
    let searchService = SearchDataService()

    func searchHotels(with data: SearchHotelData) async throws {
        do {
            let result = try await searchService.getHotels(with: data)
        } catch {
            print(#function, error.localizedDescription)
        }
    }
}

**Endpoint**

```
import APIManager

enum SearchDataServiceEndpoint: Endpoint {
    case getHotels(with: SearchHotelData)

    var path: String {
        switch self {
        case .getHotels:
            return API.Hotels.getHotels
        }
    }
    var requestType: RequestType {
        switch self {
        case .getHotels:
            return .post
        }
    }

    var header: [String : String]? {
        return [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
    }

    var parameters: [String : Any]? {
        switch self {
        case .getHotels(let data):
            return [
                "checkin": data.inDate,
                "checkout": data.outDate,
                "guests": [
                    ["adults": data.guests.adults,
                     "children": data.guests.children],
                ],
                "region_id": data.regionID
            ]
        }
    }
}
```


**Для декодирования ошибок можно расширить RequestError:**

```
    var message: String {
        switch self {
        case .unknown(let data):
            let error = data.decode(model: Model.self)
            let errorMessage = error?.errors.map { $0.compactMap { $0.message } }
            return errorMessage?.reduce("", { $0 + "\n" + $1 }) ?? "Error with request"
        case .noResponse:
            return NSLocalizedString("error.no.response", comment: "")
        case .decodingError(let error):
            return NSLocalizedString("error.decoding", comment: "") + ": \n" + error.message
        default:
            return NSLocalizedString("error.unhandled", comment: "")
        }
    }

    var errorCode: Int? {
        switch self {
        case .unknown(let data):
            let error = data.decode(model: Model.self)
            let errorCode = error?.errors.map { $0.compactMap { $0.code } }
            return errorCode?.first
        default:
            return 403
        }
    } 
```
И выводить статус об ошибки используя:

```
if let error = error as? RequestError {
     errorMessage = error.message

     или сверяя код ошибки:
    if error.errorCode == ? {
        do smth
    }
} 
```


